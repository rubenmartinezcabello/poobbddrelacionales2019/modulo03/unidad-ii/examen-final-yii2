<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciudades".
 *
 * @property int $id
 * @property string $nombre
 * @property float|null $habitantes
 * @property string|null $escudo
 * @property string|null $mapa
 */
class Ciudad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciudades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'unique'],
            [['habitantes'], 'number'],
            [['nombre', 'escudo', 'mapa'], 'string', 'max' => 255],
            [['habitantes', 'escudo', 'mapa'], 'default'], // Elementos vacios se convierten a valor por defecto (NULL)
            
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'habitantes' => 'Habitantes',
            'escudo' => 'Escudo',
            'mapa' => 'Mapa',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CiudadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CiudadQuery(get_called_class());
    }
}
