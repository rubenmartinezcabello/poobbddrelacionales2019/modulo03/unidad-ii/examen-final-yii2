<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Ciudad]].
 *
 * @see Ciudad
 */
class CiudadQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ciudad[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ciudad|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
