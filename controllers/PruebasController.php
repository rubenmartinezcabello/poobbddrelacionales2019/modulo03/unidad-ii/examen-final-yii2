<?php

namespace app\controllers;

use yii\components;

class PruebasController extends \yii\web\Controller
{
    public $layout = "pruebas";
    
    public function actionUno()
    {
        return "<p>¡Hola, mundo!</p>";
    }

    public function actionDos()
    {
        $pasatiempo = function(\DateTime $dt) { return $dt->modify("+2 days")->add(new \DateInterval('PT10M'))->format('d-m-Y h:i:s'); };
        $ahora = new \DateTime();
        return $pasatiempo($ahora);
    }
    
    public function actionTres()
    {
        return $this->render('tres');        
    }
    
    public function actionCuatro()
    {
        //"En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo corredor."
        $camion = new \app\components\Cadena("Ejemplo de camión");
        return $this->render('cuatro',[
            'Texto' => $camion->getNombre(), 
            'Numero' => $camion->getVocales(),
            'Longitud' => $camion->getLongitud()
        ]);
    }
    
                
}
