<?php

namespace app\components;

class Cadena extends \yii\base\BaseObject
{
    protected $nombre;
    protected $vocales;
    protected $longitud;

    public static function calcularVocales($str) 
    { 
        return strlen(preg_replace('/[^aeiouáéíóúüAEIOUÁÉÍÓÚÜ]/', '', $str)); 
    }
    
    public function __construct($nombre = "", $config = []) 
    { 
        parent::__construct($config);
        $this->nombre = $nombre; 
        $this->longitud = strlen($this->nombre);
        $this->vocales = self::calcularVocales($this->nombre);
    }
    
    public function getNombre() : string 
    { 
        return $this->nombre; 
    }
    public function setNombre(string $nombre) 
    {
        $this->nombre = $nombre;
        $this->longitud = strlen($this->nombre);
        $this->vocales = self::calcularVocales($this->nombre);
        return $this->nombre;
    }
    
    public function getVocales() : int
    {
        return $this->vocales;
    }
    
    public function getLongitud() : int 
    {
        return $this->longitud;
    }

}
